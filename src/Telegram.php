<?php
namespace Etsoftware\Chat;

use Etsoftware\Chat\Base;
use TelegramBot\Api\BotApi;

class Telegram extends Base
{
	
    public function __construct($token=null){
    	if ($token) {
    		$this->token = $token;
    	}
    }
	/**
	 * Send message 
	 * @param [type]  $msg       
	 * @param [type]  $args      ['chatId'=>'']
	 */
    public function sendMessage($msg, $args){
    	$args = $args??[];
        $chatId = $args['chatId']??'';
    	$data = ['errcode'=>0, 'data'=>'ok'];
        $bot = new BotApi($this->token);
        try {
            $bot->sendMessage($chatId, $msg);
        } catch (\Exception $e) {
        	$data = ['errcode'=>10001, 'data'=>$e->getMessage()];
        }
        return $data;
    }
	/**
	 * Send message 
	 * @param [type]  $msg       
	 * @param [type]  $args      ['chatId'=>'']
	 */
    public function sendDocument($file, $args){
    	$args = $args??[];
        $chatId = $args['chatId']??'';
    	$data = ['errcode'=>0, 'data'=>'ok'];
        $bot = new BotApi($this->token);
        $file = new \CURLFile($file);
        try {
            $bot->sendDocument($chatId, $document);
        } catch (\Exception $e) {
        	$data = ['errcode'=>10002, 'data'=>$e->getMessage()];
        }
        return $data;
    }
}