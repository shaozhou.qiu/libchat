<?php
namespace Etsoftware\Chat;

abstract class Base
{
    private $token = null;
	/**
	 * Send message 
	 * @param [type]  $args       
	 */
    abstract public function sendMessage($msg, $args);
}